This is a platform that is designed to sit on the nose (the sampling end) of the Tracer 5i portable XRF instrument. When the instrument is used in a lab, a convenient configuration for powders and liquid samples is to have the instrument in a stand facing upwards, with the sample balanced on the instrument nose. However, the small flat section on the instrument nose makes balancing samples quite difficult. This part is designed to provide a large (relatively) stable surface on which samples can be placed during measurements.

Two variants are provided.

A simple platform, which sits on the instrument:
Tracer5i_SamplePlatform.step
Tracer5i_SamplePlatform.stl

The same platform shape, but with embedded magnets to hold plates for centering samples in specific holders (like sample cups).
Tracer5i_SamplePlatform_Magnets.step
Tracer5i_SamplePlatform_Magnets.stl

When printing the version with embedded magnets, the print will need to be paused at a specific layer height so that the magnets can be inserted, then the print is continued. The magnets are diametrically magnetized cylindrical magnets, with a nominal 3 mm diameter and 6 mm length. The specific magnets used during testing are Hurricane Magnets & Materials Engineering NdFeB Cylinder 3*6 mm magnets, purchased through Amazon.com (27USD/200 pcs, https://www.amazon.com/gp/product/B083Q8LCXQ/). The magnet holes start 1.75 mm below the top platform surface, and have rectangular cavities 3.5*7 mm extending down a total of 7.25 mm (1.75+5.5). After print completion, the magnets are fully encapsulated, with 1 mm of plastic below the magnet cavities. The pause in the print for inserting the magnets should be placed between 8.8 and 9 mm above the print bed surface, so that the cavity is still open when the print pauses. There are spaces for 6 magnets. Two rows are positioned 100 mm apart, with magnets at 50 mm increments, centered on the instrument nose. The cylinders are aligned to the long axis of the instrument nose. The magnet hole pattern is shown in a png image in this folder.

A print profile is included for PrusaSlicer for printing the platform in PLA.

Magnetic sample cup locators have been included for the 1cc and 2cc XRF sample cups. These holders should be printed using the same 3x6 mm diametrically magnetized magnets as the platform. These sample cup locators are designed so that the IR interlock is exposed and the holder will leave the XRF sampling region in obvious view if the platform is installed in the wrong orientation.


Locator platforms are not yet correctly positioned. The hole diameters also need to be enlarged by 0.5 mm to have sufficient clearance for easy sample cup insertion.


