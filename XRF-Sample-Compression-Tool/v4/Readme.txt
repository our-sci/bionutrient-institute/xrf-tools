V4 Press Tooling

16.89 mm diameter sample aperture inside ring.
8 mm tall inside ring for sample.
Ring shoulder OD is 24 mm.
Approx. 2 cc sample to get 5 mm thick sample.

Comparison to previous (smaller) V3:
 - Approximate compressed volume is 0.56 cm^3
 - Approximate uncompressed volume is 1.0 cm^3

16.89 mm diameter ID for V4 should give:
 - 1.12 cm^3 compressed volume
 - 2.0005 cm^3 uncompressed volume

Note also that the v4 sample cup (split ring for capturing etnom film) has a shoulder contour for holding the funnel, just like the sample ring (which is the non-split ring, which doesn't capture the etnom film). In v3, the split ring does not have a shoulder to hold the funnel (the v3 sample cup wall is too thin for the tapered shoulder).

