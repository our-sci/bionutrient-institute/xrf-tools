%% Generate output data table

outputFilename = "2020XRFData.xls";

% Table containing:
% Col 1:
%   SampleID (text, eg. '11-29', often 7 characters)
% Col 2:
%   FilePath (text, eg. 'Crop He\kale\11-29.pdz')
% Col 3:
%   MeasurementDate (string, eg. "%Y-%m-%d %H:%M:%S", "2021-03-23 10:39:04")
% Col 4:
%   ProcessingDate (string, eg. "%Y-%m-%d %H:%M:%S", "2021-06-22 10:10:34")
% Col 5:
%   SampleType (string, eg. "kale")
% Col 6:
%   CropType (string, eg. "leafy greens")
% Cols (4 to (End - 3)):
%   %XX%_Concentration (decimal, eg. 1048.3)
% Col (End - 2):
%   Good (logical, 0 if potential error identified, 1 if no errors identified)
% Col (End - 1):
%   StatusCode (string, eg. "1 3 4 4 5")
% Col (End):
%   ErrorString

%% Writing file header
FileHeader = {'SampleID', 'FilePath', 'MeasurementDate', 'ProcessingDate', 'SampleType', 'CropType'};
for ix = 1:numel(ChemicalSpecies)
    FileHeader{end+1} = strcat(ChemicalSpecies{ix},'_Concentration');
end
FileHeader(end+1:end+3) = {'Good', 'StatusCode', 'ErrorString'};

xlswrite(outputFilename,FileHeader,'XRF_2020','A1:V1')


%% Writing Concentration Units Row
xlswrite(outputFilename,ConcentrationUnits,'XRF_2020','G2:S2')


%% Adding Sample-Specific Information by Columns

% Compile filenames
SampleID = {};
FilePath = {};
for ix = numel(fullData):-1:1
    [path,prefix,suffix] = fileparts(fullData(ix).sampleInfo.FileName);
    SampleID{ix} = strcat("'",prefix);
    pathparts = split(path,'\');
    pathparts{end+1} = prefix;
    FilePath(ix) = join(pathparts(end-4:end),'\');
    FilePath{ix} = join({FilePath{ix},suffix},'');
end

numSamples = numel(SampleID);
xlswrite(outputFilename,string(SampleID'),'XRF_2020',strcat('A3:A',num2str(2+numSamples)));
xlswrite(outputFilename,string(FilePath'),'XRF_2020',strcat('B3:B',num2str(2+numSamples)));

DateStrings = {};
for ix = numSamples:-1:1
    DateStrings{ix,1} = datestr(fullData(ix).sampleInfo.DateTime,'yyyy-mm-dd HH:MM:SS');
    DateStrings{ix,2} = datestr(now,'yyyy-mm-dd HH:MM:SS');
end
xlswrite(outputFilename,string(DateStrings),'XRF_2020',strcat('C3:D',num2str(2+numSamples)));

SampleType = dataFileList(:,1);
% cropType(1)

xlswrite(outputFilename,string(SampleType),'XRF_2020',strcat('E3:E',num2str(2+numSamples)));
xlswrite(outputFilename,string(cropType'),'XRF_2020',strcat('F3:F',num2str(2+numSamples)));

xlswrite(outputFilename,Concentrations,'XRF_2020',strcat('G3:S',num2str(2+numSamples)));

xlswrite(outputFilename,[ErrorStatus.good]','XRF_2020',strcat('T3:T',num2str(2+numSamples)));

ErrStringList = {};
for ix = numSamples:-1:1
    ErrStringList{ix} = num2str(ErrorStatus(ix).codeList);
end

xlswrite(outputFilename,ErrStringList','XRF_2020',strcat('U3:U',num2str(2+numSamples)));
xlswrite(outputFilename,string({ErrorStatus.errorString}'),'XRF_2020',strcat('V3:V',num2str(2+numSamples)));

