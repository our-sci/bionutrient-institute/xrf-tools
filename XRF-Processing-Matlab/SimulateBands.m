function [PureBands, BandNames] = SimulateBands( XAxis, XRFbands, InstrumentParameters)
% SimulateBands uses a set of defined bands, instrument broadening
%   functions, and x axis to generate a set of ideal band curves.
%
% Usage:
%   [PureBands, BandNames] = SimulateBands( XAxis, XRFbands, InstrumentParameters)

% InstrumentParameters.axisSpacing = 0.02; % space all data points by even steps of 0.02 keV.
%
% % Get the instrument-specific band broadening functions
% InstrumentParameters.FWHM = @(x_pos_keV) 0.011*x_pos_keV+0.074; % Band width
% InstrumentParameters.n = @(x_pos_keV) 10.^(-0.16*x_pos_keV-0.33); % Lorentzian:Gaussian fraction

for speciesIndex = 1:size(XRFbands,1)
    currSpecies = XRFbands(speciesIndex,:);
    currBands = currSpecies{2};
    currBandProfile = zeros(size(XAxis));
    for bandIndex = 1:size(currBands,1)
        bandPos = currBands{bandIndex,2};
        bandIntensity = currBands{bandIndex,3};
        bandProperties = [bandPos, InstrumentParameters.FWHM(bandPos), InstrumentParameters.n(bandPos), bandIntensity];
        currBandProfile = currBandProfile + pseudoVoigtFunction(XAxis, bandProperties(1), bandProperties(2), bandProperties(3), bandProperties(4));
    end
    PureBands(speciesIndex, :) = currBandProfile;
end
BandNames = XRFbands(:,1);

if (min(XAxis) < 19.5) && (max(XAxis) >= 18.5)
    % Add Compton band at 19 keV, if appropriate
    PureBands(end+1, :) = pseudoVoigtFunction(XAxis, 19, 0.8, 0, 100);
    BandNames{end+1,1} = 'Compton';
end

% Add a constant offset fitting term, if desired.
PureBands(end+1, :) = 1; % add an offset fitting term.
BandNames{end+1,1} = 'Offset';

% normalize simulated bands to unit area:
% make all the bands have a unit weight
for speciesIndex = 1:size(PureBands,1)
    PureBands(speciesIndex,:) = PureBands(speciesIndex,:)./sum(PureBands(speciesIndex,:));
end

end
