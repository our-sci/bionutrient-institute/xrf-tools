function [FpV] = pseudoVoigtFunction(axis, bandCenter, bandFWHM, GLratio, bandIntensity)
% pseudoVoigtFunction generates a Pseudo-Voigt band profile.
%
% Spectral lines are typically Lorentzian, but instrument broadening
% functions cause the observed lines to be shaped as a convolution of
% Gaussian and Lorentzian functions. Calculating the convolved peaks is
% computationally complicated. Instead, pseudo-Voight functions are used,
% where a linear combination of Gaussian and Lorentzian peak shapes provide
% a reasonable band shape approximation.
%
% Usage:
%   [FpV] = pseudoVoigtFunction(axis, bandCenter, bandFWHM, GLratio, bandIntensity)
%
% Where:
%   axis is the spectral axis
%   bandCenter is the band center position
%   bandFWHM is the full-width at half-max of the band
%   GLratio is the fraction Lorentzian : Gaussian for the pV band
%   bandIntensity (optional) sets a height. Default is 1.

w = bandFWHM;
x = (2*axis - 2*bandCenter)/w;

% bandFWHM = 2(ln2)1/2 G = 2 L

% Gfwhm = bandFWHM/(2*log(2)^0.5);
% Lfwhm = bandFWHM/2;

n = GLratio; % Gaussian:Lorentzian ratio

% ,Gfwhm,Lfwhm
FpV = (1-n)*Fg(x) + n*Fl(x);

if nargin>=5
    FpV = FpV*bandIntensity;
end

% Thompson et al. (1987) pseudo-Voigt calculation.

% Based on:
%   Ida, T., M. Ando, and H. Toraya. �Extended Pseudo-Voigt Function for
%   Approximating the Voigt Profile.� Journal of Applied Crystallography
%   33, no. 6 (December 1, 2000): 1311�16.
%   https://doi.org/10.1107/S0021889800010219.
%   Dasgupta, P. �On Use of Pseudo-Voigt Profiles in Diffraction Line
%   Broadening Analysis.� FIZIKA A-ZAGREB- 9, no. 2 (2000): 61�66.


end

function [i] = Fg(x) % , Gfwhm)
% Gaussian Function

% i = (1/((pi()^0.5)*Gfwhm)).^((-x.^2)/(Gfwhm^2));
i = exp(-(log(2))*x.^2);

end

function [i] = Fl(x) %, Lfwhm)
% Lorentzian Function

% i = (1/(pi()*Lfwhm))./(1+(x.^2)/(Lfwhm^2));
i = 1./(1+x.^2);

end
