function [axis, intensity, background] = SubtractBackground(axis, intensity, anchorRegions, backgroundRegions)
% SubtractBackground corrects XRF data for the background using the OS1 definitions

[bg_axis, bg_intensity] = FitBackground(axis, intensity, anchorRegions, backgroundRegions);

% interpolate the background onto the data axis
commonRange = [max(min(axis),min(bg_axis)), min(max(axis),max(bg_axis))]; % find the overlapped range
% commonRangeSelectBG = (bg_axis>=commonRange(1)) & (bg_axis<=commonRange(2)); % find the common background points
commonRangeSelectData = (axis>=commonRange(1)) & (axis<=commonRange(2)); % find the common data points
bgData = akimai(bg_axis, bg_intensity, axis(commonRangeSelectData)); % interpolate the bg onto the data

axis = axis(commonRangeSelectData);
intensity = intensity(commonRangeSelectData) - bgData;
background = bgData;
% plot(axis(commonRangeSelectData), intensity(commonRangeSelectData) - bgData);

end

