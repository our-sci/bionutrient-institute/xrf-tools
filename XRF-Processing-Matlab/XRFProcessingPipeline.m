function [Results, OutputData, DataIn] = XRFProcessingPipeline(...
    PDZFileName, ProcessingMethodList, ProcessingModel,...
    InstrumentParameters, ErrorCheckParameters, stoppingPoint)
% Processing pipeline architecture approach

% November 2021: Adding the option to pre-load and pass that parameters by
% value. This is to enable optimization of specific parameters on a per-spectrum basis.

debugFits = true;

if ~exist(PDZFileName) || isempty(PDZFileName)
    error('A PDZ file must be passed to the XRF processing pipeline.');
end

if (nargin<2) % If the function is not passed a list of processing function handles, then use the produce processing methods by default.
    % Define function handles to the data processing methods to be used
    ProcessingMethods = {@BandDefinitionsOS4, @BandDefinitionsOS3, @BandDefinitionsOS5};
else
    ProcessingMethods = ProcessingMethodList;
end

if (nargin<3)
    ProcessingModel = XRFProcModelMay2021Produce();
end

if (nargin < 4)
    % Get the required processing parameters
    InstrumentParameters.axisSpacing = 0.02; % space all data points by even steps of 0.02 keV.
    
    % Get the instrument-specific band broadening functions
    % % May 27 2021
%     InstrumentParameters.FWHM = @(x_pos_keV) 0.011*x_pos_keV+0.074; % Band width
%     InstrumentParameters.n = @(x_pos_keV) 10.^(-0.16*x_pos_keV-0.33); % Lorentzian:Gaussian fraction
    % lims = @(bandKnown) (bandKnown(1) + 1.1*bandKnown(2)*[-1,+1]);
    
    % % June 3 2021
    % Note: N band is much more Lorentzian than indicated by this function.
    % However, total area should not be affected much because it is well
    % isolated.
%     InstrumentParameters.FWHM = @(x_pos_keV) 0.012*x_pos_keV + 0.07; % Band width
    InstrumentParameters.FWHM = @(x_pos_keV) -0.00015*x_pos_keV^2 + 0.014*x_pos_keV + 0.066; % Band width
    InstrumentParameters.n = @(x_pos_keV) 10.^(-0.14*x_pos_keV-0.44); % Lorentzian:Gaussian fraction
    InstrumentParameters.NominalTubeCurrent = 15; % default nominal tube current. Used in normalization.
    InstrumentParameters.CurrentCorrection = @(x) 0.095*x + 0.05; % scaling for counts/second change due to current

% else
%     axisSpacing = InstrumentParameters.axisSpacing;
%     FWHM = InstrumentParameters.FWHM;
%     n = InstrumentParameters.n;
    
end

if (nargin < 5)
    ErrorCheckParameters = @CropErrorCheckParams2020;
end


if (nargin < 6) % If no stopping point is provided, run all processing steps.
    stoppingPoint = inf;
end

if stoppingPoint == -1
    debugFits = true;
    
end


Results = [];
DataIn = [];
OutputData = {};

%% Step 1a

% Load the data, if a file handle is passed. Otherwise check that the data
% is actually a spectrum.

if isa(PDZFileName,'string')
    % Read in data from the PDZ file
    [DataIn.Intensity, DataIn.XAxis, DataIn.sampleInfo] = read_pdz(PDZFileName);
    
elseif (isstruct(PDZFileName) && isfield(PDZFileName,'Intensity'))
    % Rename the PDZ data structure (should have .Intensity, .XAxis, .sampleInfo)
    DataIn = PDZFileName;
    
else
    error('PDZ data not correctly passed to the XRF data processing pipeline.');
    
end

% Optional field to allow the x-axis to be optimized for correction.
if isfield(InstrumentParameters, 'AxisOffsetCorrection')
    XRF.XAxis = XRF.XAxis + InstrumentParameters.AxisOffsetCorrection;
end

if debugFits == true
    fh = figure;
end
    
%% Step 1b
% Load the processing parameters:
% Process the data file with each a set of parameters:
for methodIndex = 1:numel(ProcessingMethods)
    XRF = DataIn;
    
    % Load the processing method, if passed as a function handle, otherwise
    % check that the correct parameters are included and populate the
    % correct internal parameters.
    if isA(ProcessingMethods{methodIndex}, 'function_handle')
        currentMethodFunction = ProcessingMethods{methodIndex};
        [XRFbands, anchorRegions, truncationLimits, backgroundRegions] = currentMethodFunction();
        
    elseif (isstruct(ProcessingMethods{methodIndex}) &&...
            isfield(ProcessingMethods{methodIndex},'XRFbands') &&...
            isfield(ProcessingMethods{methodIndex},'anchorRegions') &&...
            isfield(ProcessingMethods{methodIndex},'truncationLimits') &&...
            isfield(ProcessingMethods{methodIndex},'backgroundRegions') )
        
        currentMethodFunction = ProcessingMethods{methodIndex};
        XRFbands = currentMethodFunction.XRFbands;
        anchorRegions = currentMethodFunction.anchorRegions;
        truncationLimits = currentMethodFunction.truncationLimits;
        backgroundRegions = currentMethodFunction.backgroundRegions;
        
    else
        error(strcat('Processing method ',num2str(methodIndex),' not correctly provided to XRF data processing pipline.');
    
    end
    
    XRF.method = currentMethodFunction;
    
    % N Ka1 band
    % [XRFbands, anchorRegions, truncationLimits, backgroundRegions] = BandDefinitionsOS4();
    % 0.7 to 1.4 keV range bands
    % [XRFbands, anchorRegions, truncationLimits, backgroundRegions] = BandDefinitionsOS3();
    % bands > 1.4 keV
    % [XRFbands, anchorRegions, truncationLimits, backgroundRegions] = BandDefinitionsOS5();
    
    %% Step 2
    % Correct the background for the specified region:
    [XRF.XAxis, XRF.Intensity, XRF.Background] = SubtractBackground(XRF.XAxis(:), XRF.Intensity(:), anchorRegions, backgroundRegions);
    
    if stoppingPoint == 2
        continue;
    end
    
    %% Step 3
    % Resample the spectrum to the desired resolution and truncate to the desired range.
    newAxis = truncationLimits(1) : InstrumentParameters.axisSpacing : truncationLimits(2);
    [XRF.XAxis, XRF.Intensity, XRF.Background] = ResampleSpectrum(XRF.XAxis, XRF.Intensity, newAxis, XRF.Background);
    
    XRF.XAxis = newAxis;
    clear newAxis;
    
    if stoppingPoint == 3
        continue;
    end
    
    %% Step 4
    % % Normalize spectral intensity:
    %       to exposure time (transform to counts/second)
    %       to constant source current (keep in counts/second)
    
%     XRF.Intensity = XRF.Intensity; % TimeLive or TimeReal ? Changed to TimeLive on June 4, based on Bruker using TimeLive and Valid Counts for PulseDensity calculations.
%     XRF.Background = XRF.Background/XRF.sampleInfo.Time.TimeLive;
%       Changed back to TimeReal on June 8, as the calibration results are
%       much better using TimeReal. I'm not sure exactly what these two
%       exposure times represent.
    
    % % Normalize spectral intensity:
    %       divide by real time (counts/second)
    %       scale to the nominal tube current (units conserved)
    
    % scaling function for counts/second change due to current, determined
    % empirically for the XRF spectrometer by measuring counts/second
    % against drive current and finding the line of best fit relative to the nominal drive current.
    CurrentCorrection = InstrumentParameters.CurrentCorrection;
%     CurrentCorrection = @(x) x;

    correction = (1/XRF.sampleInfo.Time.TimeReal)*...
                CurrentCorrection(InstrumentParameters.NominalTubeCurrent)/CurrentCorrection(XRF.sampleInfo.TubeCurrent);
    
    XRF.Intensity = XRF.Intensity*correction;
    XRF.Background = XRF.Background*correction;
    
    if stoppingPoint == 4
        continue;
    end
    
    %% Step 5a
    % Generate simulated bands for band weight estimation:
    
    [XRF.PureBands, XRF.BandNames] = SimulateBands( XRF.XAxis, XRFbands, InstrumentParameters);
    
    %% Step 5b
    % Perform NNLS fitting to determine the appropriate
    
    % Use non-negative least squares to find more appropriate solutions:
    XRF.BandWeights = nnls(XRF.PureBands',XRF.Intensity);
%     BandNames{end+1,1} = 'Const';
%     BandWeights(:,end+1) = 1;
    
    % Make subplot for spectral subregion.
    if debugFits == true
        
        figure(fh);
        subplot(numel(ProcessingMethods),1,methodIndex);
        hold off;
        
        disp(strcat('testing method ', num2str(methodIndex), ' on file ', PDZFileName));
        % Show the band fits:
%         figure;
        plot(XRF.XAxis(:), XRF.Intensity(:),'k')
        hold on;
        plot(XRF.XAxis(:), XRF.Background(:),'c')
%         XRF.BandWeights
%         XRF.PureBands
        sumTmp = zeros(size(XRF.PureBands,2));
        for ix = 1:numel(XRF.BandWeights)
            tmp = XRF.PureBands(ix,:).*XRF.BandWeights(ix);
            sumTmp = sumTmp + tmp;
            plot(XRF.XAxis(:), XRF.PureBands(ix,:).*XRF.BandWeights(ix),'b:');
        end
        plot(XRF.XAxis(:), sumTmp','r');
        xlabel("Energy (keV)");
        ylabel("Intensity (counts)");
        
        % Calculate Residuals
        XRF.residual = sqrt(sum((XRF.Intensity(:) - sumTmp(:)).^2));
        disp(XRF.residual);
        pause;
    end
    
    OutputData{methodIndex} = XRF;
    
    if stoppingPoint == 5
        continue;
    end
    
end

if stoppingPoint <= 5
	return;
end

%% Step 6
% % Predict chemical composition from known relationships & calibration coefficients.

% Step 6a
% Combine the BandWeights from the sub-models in the output data

[BandWeights, BandNames] = GroupOutputs(OutputData);

%% Step 6b 

[ChemicalSpecies, Concentrations, ConcentrationUnits] = ApplyProcessingModel(ProcessingModel, BandWeights);


if stoppingPoint == 6
    return;
end

%% Step 7
% Estimate error for each predicted concentration


if stoppingPoint == 7
    return;
end


%% Group results

Results.BandWeights = BandWeights;
Results.BandNames = BandNames;
Results.ChemicalSpecies = ChemicalSpecies;
Results.Concentrations = Concentrations;
Results.ConcentrationUnits = ConcentrationUnits;

%% Step 8
% Compare measurements against reporting thresholds and determine reportable parameters


if stoppingPoint == 8
    return;
end


%% Step 9
% perform validity checks for user warnings.

[Results.ErrorStatus] = ErrorCheck(Results, OutputData, ErrorCheckParameters());

if stoppingPoint == 9
    return;
end


end

