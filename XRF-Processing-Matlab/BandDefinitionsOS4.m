function [XRFbands, anchorRegions, truncationLimits, backgroundRegions] = BandDefinitionsOS4()
% The new OurSci XRF band definitions.

% For analyzing the N Ka1 band in produce only.

% Species, {Emission line name, band center, Relative intensity}
XRFbands = {...
    'N-K', {'Ka1', 0.3924, 100}};

% Background anchor regions:
%   [rangeStart, rangeEnd, type]
anchorRegions = {...
    0.15, 0.25, 'all';... % mean
    0.55, 0.65, 'all'}; % mean

% Truncation Limits:
truncationLimits = [ 0.2, 0.6];

% Regions defined as: [start, stop, polynomial order]
backgroundRegions = {...
    0.1, 0.7, 1}; % linear fit

end
