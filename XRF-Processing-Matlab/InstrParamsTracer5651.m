function [InstrumentParameters] = InstrParamsTracer5651()
% [InstrumentParameters] = InstrParamsTracer5651()
% Tracer 5i serial number "SK5-5651" 

    InstrumentParameters.axisSpacing = 0.02; % space all data points by even steps of 0.02 keV.
    
    % % June 3 2021
    % Note: N band is much more Lorentzian than indicated by this function.
    % However, total area should not be affected much because it is well
    % isolated.
    InstrumentParameters.FWHM = @(x_pos_keV) -0.00015*x_pos_keV^2 + 0.014*x_pos_keV + 0.066; % Band width
    InstrumentParameters.n = @(x_pos_keV) 10.^(-0.14*x_pos_keV-0.44); % Lorentzian:Gaussian fraction
    InstrumentParameters.NominalTubeCurrent = 15; % 15 microAmps
    InstrumentParameters.CurrentCorrection = @(x) 0.095*x + 0.05; % Function for scaling of counts/second versus tube drive current
    
end
