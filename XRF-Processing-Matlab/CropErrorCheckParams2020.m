function [ErrorCheckParameters] = CropErrorCheckParams2020()

% ErrorCheckParameters is a cell array of cell arrays. Each sub-cell array
% contains one set of parameters to use in checking the current sample.

% Source Type (concentration species, band weight, other property in OutputData.sampleInfo)
% Data Source Name
% Data Source Index
% Comparison function (lt, le, gt, ge, eq)
% Comparison Value
% Error/Warning string
% Response Code:
%   1: concentration out of expected range
%   2: background atmospheric flush seems to be missing
%   3: Lower than expected signal level
%   4: Potential sample contamination
%   5: Incorrect detector temperature
%   6: Incorrect total measurement time

ErrorCheckParameters = {...
    {'Concentration', 'Ni', 11, @lt, -5, 'Ni < -5 ppm', 1},...
    {'Concentration', 'Fe', 10, @gt, 250, 'Fe > 250 ppm', 1},... % soil
    {'Concentration', 'Si', 3, @gt, 15000, 'Si > 15000 ppm', 1},... % soil
    {'BandWeight', 'N-K', 1, @gt, 1100, 'N-K > 1100 cps', 2},... % missing helium atmosphere
    {'BandWeight', 'Cu-L', 5, @lt, 10, 'Cu-L < 10 cps', 3},...
    {'BandWeight', 'Mg-K', 9, @lt, 2, 'Mg-K < 2 cps', 3},...
    {'BandWeight', 'Al-K', 9, @gt, 150, 'Al-K > 150 cps', 4},... % soil ?
    {'BandWeight', 'Si-K', 13, @gt, 750, 'Si-K > 750 cps', 4},... % soil
    {'BandWeight', 'P-K', 15, @gt, 3000, 'P-K > 3000 cps', 4},... % fertilizer
    {'BandWeight', 'P-K', 15, @lt, 50, 'P-K < 50 cps', 3},...
    {'BandWeight', 'S-K', 17, @lt, 100, 'S-K < 100 cps', 3},...
    {'BandWeight', 'Cl-K', 18, @lt, 100, 'Cl-K < 100 cps', 3},...
    {'BandWeight', 'Rh-L', 19, @lt, 1000, 'Rh-L < 1000 cps', 3},...
    {'BandWeight', 'Ar-K', 20, @lt, 200, 'Ar-K < 200 cps', 2},...
    {'BandWeight', 'K-K', 21, @lt, 1500, 'K-K < 1500 cps', 3},...
    {'BandWeight', 'Ti-K', 23, @gt, 100, 'Ti-K > 100 cps', 4},... % soil
    {'BandWeight', 'Mn-K', 24, @lt, 20, 'Mn-K < 20 cps', 3},...
    {'BandWeight', 'Fe-K', 25, @lt, 100, 'Fe-K < 100 cps', 3},...
    {'BandWeight', 'Fe-K', 25, @gt, 3000, 'Fe-K > 3000 cps', 4},... % soil
    {'BandWeight', 'Co-K', 26, @lt, 10, 'Co-K < 10 cps', 3},...
    {'BandWeight', 'Ni-K', 28, @lt, 50, 'Ni-K < 50 cps', 3},...
    {'BandWeight', 'Cu-K', 29, @lt, 15, 'Cu-K < 15 cps', 3},...
    {'BandWeight', 'Br-K', 31, @gt, 300, 'Br-K > 300 cps', 4},... % agricultural chemicals & pesticides
    {'SampleInfo', 'ValidCounts', 0, @lt, 1000000, 'valid counts < 1000000 counts', 3},... % Insufficient valid counts
    {'SampleInfo', 'PulseDensity', 0, @lt, 20000, 'pulse density < 20000 cps', 3},... % Sample grinding or compression issue
    {'SampleInfo', 'DetectorTemperature', 0, @ne, 202, 'detector temperature != 202', 5},... % Detector temperature is not 202
    {'SampleTime', 'TimeTotal', 0, @lt, 28, 'total time <28 s', 6},... % Sample measurement time is not between 28 and 29 seconds
    {'SampleTime', 'TimeTotal', 0, @gt, 29, 'total time >29 s', 6}... % Sample measurement time is not between 28 and 29 seconds
    };

end
