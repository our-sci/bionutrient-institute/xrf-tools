function [bg_axis, bg_intensity] = FitBackground(axis, intensity, anchorRegions, backgroundRegions)

axis = axis(:);
intensity = intensity(:);

if (nargin<3)
    [~, anchorRegions, ~, backgroundRegions] = BandDefinitionsOS1();
end

% these are the pixel ranges that aren't affected by element bands
[backgroundIndices] = GetAnchorData(axis, intensity, anchorRegions);

% PlotBands(axis, intensity, XRFbands)
x_pieces = {};
y_pieces = {};

% Find background regions, as defined by lower and upper bounds
for bg_index = 1:size(backgroundRegions,1)
    bg_start = backgroundRegions{bg_index,1};
    bg_end = backgroundRegions{bg_index,2};
    bg_order = backgroundRegions{bg_index,3};
    
    % select the portions of the data that should be part of the background
    current_axis = axis(backgroundIndices);
    current_data = intensity(backgroundIndices);
    
    % select only the current background region of interest
    selected_bg_region = (current_axis >= bg_start)&(current_axis <= bg_end);
    
    % fit a polynomial to the current background range
%     plot(current_axis(selected_bg_region), current_data(selected_bg_region),'r-')
    [p,S,mu] = polyfit(current_axis(selected_bg_region), current_data(selected_bg_region),bg_order);
    
    local_axis = axis((axis >= bg_start)&(axis <= bg_end));
    [y_est] = polyval(p,local_axis,S,mu);
%     plot(local_axis, y_est,'r-')
    x_pieces{end+1} = local_axis;
    y_pieces{end+1} = y_est;
    % x_pieces and y_pieces now contain the relevant chunk of x positions
    % and associated polynomial interpolated background intensity
end

% PlotBands(axis, intensity, XRFbands)
% plot(axis(backgroundIndices),  intensity(backgroundIndices),'b--');

% join the background regions using a logistic function
merge_axis = 0:0.05:1;
[merge_values] = LogisticFunction(merge_axis, 0.5, 10, 1);
% The logistic weighting function is used to smoothly merge different polynomial curve regions

for bg_index = (size(backgroundRegions,1)-1):-1:1
    % for each background region
    bg1_end = backgroundRegions{bg_index,2};
    bg2_start = backgroundRegions{bg_index+1,1};
    
    bg_range = bg1_end - bg2_start; % the x position range for the current background region
    ma = bg2_start + merge_axis*bg_range; % scaled logistic function x axis (merge axis)
    
    % get the x and y data for the fitted background region
    x1 = x_pieces{bg_index};
    x2 = x_pieces{bg_index+1};
    y1 = y_pieces{bg_index};
    y2 = y_pieces{bg_index+1};
    
    % find the overlapping region and smoothly merge the background estimates 
    % using a logistic function
    [common_region,ia,ib] = intersect(x1,x2);
    y2_scaling = akimai(ma, merge_values, common_region);
    common_intensity = y2(ib).*y2_scaling + y1(ia).*(1-y2_scaling);
    
    % find the regions that don't overlap, and merge the background points
    [~,ia,ib] = setxor(x1,x2);
    diff_region = [x1(ia); x2(ib)];
    diff_intensity = [y1(ia); y2(ib)];
    
    % combine the regions, and ensure they're sorted.
    [newaxis, sortindices] = sort([common_region; diff_region]);
    newvalues = [common_intensity; diff_intensity];
    newvalues = newvalues(sortindices);
    
    % remove the higher value
    x_pieces(bg_index+1) = [];
    y_pieces(bg_index+1) = [];
    
    % store the merged background estimate into the lower indexed region
    x_pieces{bg_index} = newaxis;
    y_pieces{bg_index} = newvalues;
end

bg_axis = x_pieces{1};
bg_intensity = y_pieces{1};

end
