function [pixelIndices] = GetAnchorData(axis, data, anchorRegions)
%
% 
% Usage:
%    [bgAxis, bgData, pixelIndices] = GetAnchorData(axis, data, anchorRegions)
%
% Inputs:
%   axis, spectral positions size [n,1]
%   data, spectral intensities size [n,1]
%   anchorRegions, defined background regions. Size [k,3]
%       where each row has a region start, stop, and type defined.
%
% Outputs:
%   pixelIndices is the selected background pixel indices


szAnchor = size(anchorRegions);
pixelIndices = [];

for regionIndex = 1:szAnchor(1)
    regionStart = anchorRegions{regionIndex, 1};
    regionEnd = anchorRegions{regionIndex, 2};
    regionType = anchorRegions{regionIndex, 3};
    
    startIndex = nearest(axis, regionStart, 'next');
    endIndex = nearest(axis, regionEnd, 'prev');
    localIndices = startIndex:endIndex;
            
    switch regionType
        case 'all'
            pixelIndices = [pixelIndices, localIndices];
            
        case 'min'
            [~, minPosition] = min(data(localIndices));
            pixelIndices = [pixelIndices, localIndices(minPosition)];
            
        otherwise
            error('The specified anchorRegion type was not valid.');
    end
    
end

end
