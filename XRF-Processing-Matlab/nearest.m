function [index] = nearest(values, target, type)
% Function nearest
%
% Find the nearest value in a list of numbers.
%
% Usage: index = nearest(values, target, type)
%
% Inputs:
%   values should be a vector of numbers
%   target is the desired value from the array
%   type is an optional parameter with the match type:
%       'closest' is the absolute nearest value (default type)
%       'nearest-below' or 'prev' is the next lower value (value < target)
%       'nearest-above' or 'next' is the next higher value (value > target)
% 
% Example: index = nearest( xaxis(:,i), 2936); % find the index of the 
%       % nearest wavenumber point to 2936 cm -1


% Francis Esmonde-White, April 25, 2009

if nargin <3
    type = 'closest';
end


switch type
    case 'closest'
        [~,index]=min( abs(values - target) );
    case {'nearest-below', 'prev'}
        [values,jInd] = sort(values);
        [iInd]=find( values < target, 1, 'last' );
        index = jInd(iInd);
    case {'nearest-above', 'next'}
        [values,jInd] = sort(values);
        [iInd]=find( values > target, 1, 'first' );
        index = jInd(iInd);
    otherwise
        error('The type of nearest match is not valid.');
end


end

