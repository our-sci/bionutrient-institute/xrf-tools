function [newAxis, intensity, background] = ResampleSpectrum(axis, intensity, newAxis, background)
% SubtractBackground resamples a spectrum onto a new x-axis to allow
%   spectra to be combined into a 2D matrix.
%
% Usage:
%   [newAxis, intensity, background] = ResampleSpectrum(axis, intensity, newAxis, background)
%
% Where:
%   axis is the original spectral axis
%   intensity is the spectral intensity
%   newAxis is the desired spectral axis
%   background (optional) is a second spectrum to interpolate.
%

intensity = akimai(axis, intensity, newAxis);

if nargin<3
    background = [];
else
    background = akimai(axis, background, newAxis);
end

end

