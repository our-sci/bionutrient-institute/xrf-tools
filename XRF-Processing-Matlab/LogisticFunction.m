function [values] = LogisticFunction(x, x0, k, L)
% Logistic function, see wikipedia:
%   https://en.wikipedia.org/wiki/Logistic_function
%
% Usage:
%   [values] = LogisticFunction(x, x0, k, L)
%
% Where:
%   x is the axis
%   x0 is the center
%   k is the width
%   L is the intensity (1)
%
% Example:
%   [values] = LogisticFunction(0:0.05:1, 0.5, 10, 1);
%   figure; plot(0:0.05:1, values)

values = L ./ ( 1 + exp(-k.*( x - x0)));


end

