function [BandWeights, BandNames] = GroupOutputs(OutputData)

BandWeights = [];
BandNames = {};
for ix = 1:numel(OutputData)
    BandWeights = [BandWeights; OutputData{ix}.BandWeights];
    BandNames = [BandNames; OutputData{ix}.BandNames];
end

end

