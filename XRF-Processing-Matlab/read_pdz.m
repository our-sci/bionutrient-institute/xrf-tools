function [IntensityData, XAxis, FileInfo] = read_pdz(filename)
%READ_PDZ Load data from Bruker PDZ binary file format
%   Created conversion function from scratch via hex editors and data
%   converted/exported using Bruker ARTAX software. Data from Tracer 5i
%   instrument.
%
% Usage: [IntensityData, XAxis, FileInfo] = read_pdz(filename);
%   FileInfo is a structure with the known data fields deciphered.
%
% Example:
%   filename = "../../ExampleData/XRF-veg/12-95.pdz"
%   [IntensityData, XAxis, FileInfo] = read_pdz(filename);
%   plot(XAxis, IntensityData)
%   axis tight
%   xlabel(FileInfo.xlabel+" ("+FileInfo.xunits+")")
%   ylabel(FileInfo.ylabel+" ("+FileInfo.yunits+")")
%   [~,name,ext] = fileparts(filename);
%   title("XRF Data File "+name+ext);
%
% Version 1.01, June 4 2021 by Francis Esmonde-White
%   Decoded additional parameters, like acquisition date.
%   Fixed a couple of mislabeled records (detector temperature was incorrect).
% 
% Version 1.02, June 7 2021 by Francis Esmonde-White
%   Added support for the smaller PDZ files that don't have extra note
%   fields.

if ~exist(filename, 'file') == 2
    error('The specified data file does not exist');
end

[~, ~, extension] = fileparts(filename);
s = dir(filename);
filesize = s.bytes;
if (~strmatch(lower(extension), '.pdz') | (filesize <= 8549))
    error('read_pdz can only read Bruker Tracer 5i *.pdz files. These files should be at least 8549 bytes long.');
    % Note: Some Tracer pdz files seem to be 8459 bytes, others have an
    % extra set of data that denotes the operator name, ID, and other text
    % data. I don't know what makes this data be saved, it appears in some
    % of the test data, and not in other data.
end

% fileID = fopen(filename,permission,machinefmt,encodingIn);
fileID = fopen(filename);

FileInfo = struct();
FileInfo.FileName = filename;

fseek(fileID, 6, 'bof'); % Skip to pixel count
NumPixels = fread(fileID,1,'uint16');
FileInfo.NumPixels = NumPixels;

fseek(fileID, 30, 'bof'); % Skip to Detector Temperature (in counts?)
DetectorTemperature = fread(fileID,1,'uint8');
FileInfo.DetectorTemperature = DetectorTemperature;
% 201 is -27.5 deg C
% 202 is -27.0 deg C
% It seems to take a few minutes to be stable at -27.0 deg C. When at
% -27.5, it's extremely obvious that it isn't stabilized, and has lower
% response than when it is later stabilized.

fseek(fileID, 32, 'bof'); % Skip to Ambient Temperature (in deg F)
AmbientTemperature = fread(fileID,1,'int16')/10;
FileInfo.AmbientTemperature = AmbientTemperature;
% The ambient temperature increases with time, and eventually gets to about 110F.
%   This isn't as obvious as the detector temperature, but may still change
%   the performance.

fseek(fileID, 50, 'bof'); % Skip to X axis Linear Step
XLinearStep = fread(fileID,1,'float64');
FileInfo.XLinearStep = XLinearStep; % in eV

fseek(fileID, 58, 'bof'); % Skip to X axis Offset
XOffset = fread(fileID,1,'float64');
FileInfo.XOffset = XOffset;

% fseek(fileID, 66, 'bof'); % Skip to ??? 0.00879628205694334 / 0.0438396396368482
% XOffset = fread(fileID,1,'float64');
% FileInfo.XOffset = XOffset;




fseek(fileID, 146, 'bof'); % Skip to Acquision Date
Year = fread(fileID,1,'uint16'); % 146
Month = fread(fileID,1,'uint16'); % 148
Unknown = fread(fileID,1,'uint16'); %150
Day = fread(fileID,1,'uint16'); % 152
Hour = fread(fileID,1,'uint16'); % 154
Minute = fread(fileID,1,'uint16'); % 156
Second = fread(fileID,1,'uint16'); % 158
FileInfo.Date = struct();
FileInfo.Date.Year= Year;
FileInfo.Date.Month= Month;
FileInfo.Date.Day= Day;
FileInfo.Date.Hour= Hour;
FileInfo.Date.Minute= Minute;
FileInfo.Date.Second= Second;

FileInfo.DateTime = datetime(Year,Month,Day,Hour,Minute,Second);

% Measured Source Tube Voltage and Current
fseek(fileID, 162, 'bof'); % Skip to high voltage ADC
HighVoltage = fread(fileID,1,'float32');
FileInfo.HighVoltage = HighVoltage;

fseek(fileID, 166, 'bof'); % Skip to tube current ADC
TubeCurrent = fread(fileID,1,'float32');
FileInfo.TubeCurrent = TubeCurrent;

% Source Tube Settings
fseek(fileID, 170, 'bof'); % Skip to HV DAC
HVDAC = fread(fileID,1,'uint8');
FileInfo.HVDAC = HVDAC;

fseek(fileID, 171, 'bof'); % Skip to Filament Current DAC
FCDAC = fread(fileID,1,'uint8');
FileInfo.FCDAC = FCDAC;

% fseek(fileID, 172, 'bof'); % Skip to ??? maybe atmosphere - 0
% Atmosphere = fread(fileID,1,'uint8');
% FileInfo.Atmosphere = Atmosphere;

fseek(fileID, 173, 'bof'); % Skip to Pulse Length
PulseLength = fread(fileID,1,'uint8');
FileInfo.PulseLength = PulseLength;

fseek(fileID, 174, 'bof'); % Skip to Pulse Current
PulseCurrent = fread(fileID,1,'uint8');
FileInfo.PulseCurrent = PulseCurrent;

fseek(fileID, 175, 'bof'); % Skip to Filter Index
FilterIndex = fread(fileID,1,'uint8');
FileInfo.FilterIndex = FilterIndex;

% fseek(fileID, 178, 'bof'); % Skip to Energy Linear (? X axis step size is repeated again?)
% XLinearStep = fread(fileID,1,'float64');
% FileInfo.XLinearStep = XLinearStep;

% fseek(fileID, 182, 'bof'); % Skip to ?(? Some kind of time ? 2.81234216690063 )
% XLinearStep = fread(fileID,1,'float32');
% FileInfo.XLinearStep = XLinearStep;

fseek(fileID, 186, 'bof'); % Skip to serial number
SerialNumber = fread(fileID,8,'char');
SerialNumber = string(char(SerialNumber'));
FileInfo.SerialNumber = SerialNumber;

fseek(fileID, 202, 'bof'); % Skip to Raw Count Last Packet
RawCountsLP = fread(fileID,1,'uint32');
FileInfo.RawCountsLP = RawCountsLP;

fseek(fileID, 206, 'bof'); % Skip to Valid Count Last Packet
ValidCountsLP = fread(fileID,1,'uint32');
FileInfo.ValidCountsLP = ValidCountsLP;


% fseek(fileID, 215, 'bof'); % Skip to ? 2.000001333
% ValidCountsLP = fread(fileID,1,'uint32');
% FileInfo.ValidCountsLP = ValidCountsLP;


% Ambient settings?
% fseek(fileID, 246, 'bof'); % Skip to ambient temperature in K?
% AmbientTemp = fread(fileID,1,'float32');
% fseek(fileID, 250, 'bof'); % Skip to ambient pressure is kpa?
% AmbientPress = fread(fileID,1,'float32');
% fseek(fileID, 255, 'bof'); % Skip to ambient pressure is kpa?
% AmbientPress = fread(fileID,1,'float32');


fseek(fileID, 322, 'bof'); % Skip to raw accumulated counts
RawCounts = fread(fileID,1,'uint32');
FileInfo.RawCounts = RawCounts;

fseek(fileID, 326, 'bof'); % Skip to valid accumulated counts
ValidCounts = fread(fileID,1,'uint32');
FileInfo.ValidCounts = ValidCounts;

fseek(fileID, 338, 'bof'); % Skip to Total time
TimeTotal = fread(fileID,1,'float32'); % 338
TimeReal = fread(fileID,1,'float32'); % 342
TimeUNK1 = fread(fileID,1,'float32'); % 346
TimeUNK2 = fread(fileID,1,'float32'); % 350
TimeLive = fread(fileID,1,'float32'); % 354, Lifetime in SPX files.
FileInfo.Time = struct();
FileInfo.Time.TimeTotal= TimeTotal;
FileInfo.Time.TimeReal= TimeReal;
FileInfo.Time.TimeUNK1= TimeUNK1;
FileInfo.Time.TimeUNK2= TimeUNK2;
FileInfo.Time.TimeLive= TimeLive;

FileInfo.PulseDensity = FileInfo.ValidCounts / FileInfo.Time.TimeLive; % Counts per second

fseek(fileID, 358, 'bof'); % Skip to Spectral Data
IntensityData = fread(fileID,NumPixels,'uint32');

fclose(fileID);

XAxis = XOffset + (0:(NumPixels-1))'*XLinearStep;
XAxis = XAxis/1000; % convert from eV to keV

FileInfo.xlabel = "Energy";
FileInfo.xunits = "keV";
FileInfo.ylabel = "Intensity";
FileInfo.yunits = "Counts";

end
