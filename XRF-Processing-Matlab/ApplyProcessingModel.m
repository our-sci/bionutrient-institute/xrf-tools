function [ChemicalSpecies, Concentrations, ConcentrationUnits] = ApplyProcessingModel(ProcessingModel, BandWeights)
% Assumes the bandweights are in the correct order
%   (processed using same order as ProcessingModel parameters)

for modelIndex = 1:numel(ProcessingModel)
    
    ChemicalSpecies{modelIndex} = ProcessingModel(modelIndex).speciesName;
    Concentrations(modelIndex) = PredictConcentration(ProcessingModel(modelIndex), BandWeights);
    ConcentrationUnits{modelIndex} = ProcessingModel(modelIndex).speciesUnits;
    
end

end

function [pred] = PredictConcentration(model, BandWeights)
% Use coefficients and band weights (measured counts) to predict a
% concentration using a linear or power model.

meas = BandWeights(model.bandIndices,:)';

for indPower = 2:model.Power
    meas = [meas, (BandWeights(model.bandIndices,:)').^indPower];
end

if model.Constant
    meas = [meas, ones(size(meas,1),1)]; % add a column of ones for providing offset coefficients
end

% Least Squares Best Fit
% coefs = (known'/meas')';
pred = (model.coefs'*meas')';

end