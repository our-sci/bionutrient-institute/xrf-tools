function [SampleErrorResult] = ErrorCheck(Results, OutputData, ErrorCheckParameters)



% ErrorCheckParameters is a cell array of cell arrays. Each sub-cell array
% contains one set of parameters to use in checking the current sample.

% Source Type (concentration species, band weight, other property in OutputData.sampleInfo)
% Data Source Name
% Data Source Index
% Comparison function (lt, le, gt, ge, eq)
% Comparison Value
% Error/Warning string
% Response Code:
%   1: concentration out of expected range
%   2: background atmospheric flush seems to be missing
%   3: Lower than expected signal level
%   4: Potential sample contamination
%   5: Incorrect detector temperature
%   6: Incorrect total measurement time

% % Example ErrorCheckParameters from 2020 XRF Crop Data
% 
% ErrorCheckParameters = {...
%     {'Concentration', 'Ni', 11, @lt, -5, 'Ni < -5 ppm', 1},...
%     {'Concentration', 'Fe', 10, @gt, 250, 'Fe > 250 ppm', 1},... % soil
%     {'Concentration', 'Si', 3, @gt, 15000, 'Si > 15000 ppm', 1},... % soil
%     {'BandWeight', 'N-K', 1, @gt, 1100, 'N-K > 1100 cps', 2},... % missing helium atmosphere
%     {'BandWeight', 'Cu-L', 5, @lt, 10, 'Cu-L < 10 cps', 3},...
%     {'BandWeight', 'Mg-K', 9, @lt, 2, 'Mg-K < 2 cps', 3},...
%     {'BandWeight', 'Al-K', 9, @gt, 150, 'Al-K > 150 cps', 4},... % soil ?
%     {'BandWeight', 'Si-K', 13, @gt, 750, 'Si-K > 750 cps', 4},... % soil
%     {'BandWeight', 'P-K', 15, @gt, 3000, 'P-K > 3000 cps', 4},... % fertilizer
%     {'BandWeight', 'P-K', 15, @lt, 50, 'P-K < 50 cps', 3},...
%     {'BandWeight', 'S-K', 17, @lt, 100, 'S-K < 100 cps', 3},...
%     {'BandWeight', 'Cl-K', 18, @lt, 100, 'Cl-K < 100 cps', 3},...
%     {'BandWeight', 'Rh-L', 19, @lt, 1000, 'Rh-L < 1000 cps', 3},...
%     {'BandWeight', 'Ar-K', 20, @lt, 200, 'Ar-K < 200 cps', 2},...
%     {'BandWeight', 'K-K', 21, @lt, 1500, 'K-K < 1500 cps', 3},...
%     {'BandWeight', 'Ti-K', 23, @gt, 100, 'Ti-K > 100 cps', 4},... % soil
%     {'BandWeight', 'Mn-K', 24, @lt, 20, 'Mn-K < 20 cps', 3},...
%     {'BandWeight', 'Fe-K', 25, @lt, 100, 'Fe-K < 100 cps', 3},...
%     {'BandWeight', 'Fe-K', 25, @gt, 3000, 'Fe-K > 3000 cps', 4},... % soil
%     {'BandWeight', 'Co-K', 26, @lt, 10, 'Co-K < 10 cps', 3},...
%     {'BandWeight', 'Ni-K', 28, @lt, 50, 'Ni-K < 50 cps', 3},...
%     {'BandWeight', 'Cu-K', 29, @lt, 15, 'Cu-K < 15 cps', 3},...
%     {'BandWeight', 'Br-K', 31, @gt, 300, 'Br-K > 300 cps', 4},... % agricultural chemicals & pesticides
%     {'SampleInfo', 'ValidCounts', 0, @lt, 1000000, 'valid counts < 1000000 counts', 3},... % Insufficient valid counts
%     {'SampleInfo', 'PulseDensity', 0, @lt, 20000, 'pulse density < 20000 cps', 3},... % Sample grinding or compression issue
%     {'SampleInfo', 'DetectorTemperature', 0, @ne, 202, 'detector temperature != 202', 5},... % Detector temperature is not 202
%     {'SampleTime', 'TimeTotal', 0, @lt, 28, 'total time <28 s', 6},... % Sample measurement time is not between 28 and 29 seconds
%     {'SampleTime', 'TimeTotal', 0, @gt, 29, 'total time >29 s', 6}... % Sample measurement time is not between 28 and 29 seconds
%     };

%     Concentrations(sampleIndex,:) = Results.Concentrations;
%     BandWeights(sampleIndex,:) = Results.BandWeights';
% ConcentrationUnits = Results.ConcentrationUnits;
% ChemicalSpecies = Results.ChemicalSpecies;
% BandNames = Results.BandNames';

SampleErrorResult.good = true;
SampleErrorResult.codeList = [];
SampleErrorResult.errorString = '';

for ix = 1:numel(ErrorCheckParameters)
    currCheckParameters = ErrorCheckParameters{ix};
    
    currParam = currCheckParameters{2};
    checkFunction = currCheckParameters{4};
    comparisonValue = currCheckParameters{5};
    
    switch lower(currCheckParameters{1})
        case 'concentration'
            % values: Results.Concentrations
            % labels: Results.ChemicalSpecies
            index = strmatch(currParam, Results.ChemicalSpecies);
            value = Results.Concentrations(index);
            
        case 'bandweight'
            % values: Results.BandWeights
            % labels: Results.BandNames
            
            index = strmatch(currParam, Results.BandNames);
            value = Results.BandWeights(index);
            
        case 'sampleinfo'
            % values: OutputData{1}.sampleInfo.(currParam)
            
            value = OutputData{1}.sampleInfo.(currParam);
            
        case 'sampletime'
            % values: OutputData{1}.sampleInfo.Time.(currParam)
            
            value = OutputData{1}.sampleInfo.Time.(currParam);
          
        otherwise
            error('Incorrect error checking parameters.');
            
    end
    
    checkResult = checkFunction(value, comparisonValue);
    
    
    if checkResult == true
        SampleErrorResult.good = false;
        currErrorStr = currCheckParameters{6};
        currErrorCode = currCheckParameters{7};
        if ~isempty(SampleErrorResult.errorString)
            SampleErrorResult.errorString = strcat(SampleErrorResult.errorString, ", ");
        end
        SampleErrorResult.errorString = strcat(SampleErrorResult.errorString, currErrorStr);
        
        SampleErrorResult.codeList = [SampleErrorResult.codeList, currErrorCode];
    end
    
    
end





% Outlier Filters

% Concentration Measures:
% [Ni] < -5 ppm
% [Fe] > 250 ppm
% [Si] > 15000 ppm

% Band Weight Measures:
% N-K > 1100
% Cu-L < 10
% Mg-K < 2
% Al-K < 150
% Si-K>750
% P-K > 3000, P-K < 50
% S-K < 100
% Cl-K < 100
% Rh-L < 1000
% Ar-K < 200
% K-K < 1500
% Ti-K > 100
% Mn-K < 20
% Fe-K < 100, Fe-K > 3000
% Co-K < 10
% Ni-K < 50
% Cu-K < 15
% Br-K >300


% ??? Additional potential issues
% Si-K < 80 is a distinct group in Oats (?) for example, 11-3597.3,
% 11-5134.3, 11-5140-1, 11-3585.3 and potato 11-1436, 11-1406
% Ar-K > 800 is a distinct group in Oat Potatoes and others.
% ESCCaKa1SiKa1 > 400 ?
% SUMK Ka1CaKB1 < 50



% Pulse Density < 20000


% for ix = 1:numel(ChemicalSpecies); disp(strcat(num2str(ix),": ", ChemicalSpecies{ix})); end
% 1: Mg
% 2: Al
% 3: Si
% 4: P
% 5: S
% 6: Cl
% 7: K
% 8: Ca
% 9: Mn
% 10: Fe
% 11: Ni
% 12: Cu
% 13: Zn

% for ix = 1:numel(BandNames); disp(strcat(num2str(ix),": ", BandNames{ix})); end
% 1: N-K
% 2: Offset
% 3: Fe-L
% 4: Ni-L
% 5: Cu-L
% 6: Na-K
% 7: Na-K
% 8: Ga-K
% 9: Mg-K
% 10: Offset
% 11: Al-K
% 12: ESCK Ka1SiKa1
% 13: Si-K
% 14: ESCCaKa1SiKa1
% 15: P-K
% 16: ESCCaKb1SiKa1
% 17: S-K
% 18: Cl-K
% 19: Rh-L
% 20: Ar-K
% 21: K-K
% 22: Ca-K
% 23: Ti-K
% 24: Mn-K
% 25: Fe-K
% 26: Co-K
% 27: SUMK Ka1CaKa1
% 28: Ni-K
% 29: Cu-K
% 30: Zn-K
% 31: Br-k
% 32: Rb-K
% 33: Sr-K
% 34: Mo-K
% 35: SUMK Ka1CaKb1
% 36: Compton
% 37: Offset

% Source Type (concentration species, band weight, other property in OutputData.sampleInfo)
% Data Source Name
% Data Source Index
% Comparison function (lt, LE, gt, GE)
% Comparison Value
% Error/Warning string
% Response Code:
%   1: concentration out of expected range
%   2: background atmospheric flush seems to be missing
%   3: Lower than expected signal level
%   4: Potential sample contamination